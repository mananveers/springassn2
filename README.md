# SpringAssn2

Contains the source code required for the following requirements

```
Spring MVC Project

--- Complete MVC project
--- Add Validations to Empl Fields
--- Add Centralised Exception Handling Class
```

The directory is not set up to be run as-is.

Classes need to be added to a working workplace project with dependencies to be executed.