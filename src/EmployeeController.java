import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EmployeeController {
    
    @GetMapping("/employee")
    public String showForm(Model model) {
        model.addAttribute("employee", new Employee());
        return "employee-form";
    }
    
    @PostMapping("/employee")
    public String processForm(@Validated @ModelAttribute("employee") Employee employee,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "employee-form";
        } else {
            return "employee-confirmation";
        }
    }
    
}
