import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class Employee {
    
    @Min(value = 1, message = "Employee ID must be greater than or equal to 1")
    private int id;
    
    @NotBlank(message = "Employee name cannot be blank")
    private String name;
    
    public Employee() {}
    
    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
