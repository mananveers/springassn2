<!DOCTYPE html>
<html>
<head>
    <title>Employee List</title>
</head>
<body>
    <h1>Employee List</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th></th>
        </tr>
        <c:forEach var="employee" items="${employees}">
            <tr>
                <td>${employee.id}</td>
                <td>${employee.name}</td>
                <td><a href="/employee/${employee.id}">View Details</a></td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <a href="/employee/new">Add New Employee</a>
</body>
</html>
