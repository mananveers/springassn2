<!DOCTYPE html>
<html>
<head>
    <title>Employee Details</title>
</head>
<body>
    <h1>Employee Details</h1>
    <table>
        <tr>
            <td>ID:</td>
            <td>${employee.id}</td>
        </tr>
        <tr>
            <td>Name:</td>
            <td>${employee.name}</td>
        </tr>
    </table>
    <br>
    <a href="/employee/edit/${employee.id}">Edit</a> | 
    <a href="/employee/delete/${employee.id}">Delete</a> |
    <a href="/employee">Back to List</a>
</body>
</html>
