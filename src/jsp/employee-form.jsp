<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>${pageTitle}</title>
</head>
<body>
    <h1>${pageTitle}</h1>
    <form action="${formAction}" method="post">
        <table>
            <tr>
                <td>ID:</td>
                <td><input type="text" name="id" value="${employee.id}" readonly="readonly" /></td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" value="${employee.name}" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Save" /></td>
            </tr>
        </table>
    </form>
    <br>
    <a href="/employee">Cancel</a>
</body>
</html>
    

