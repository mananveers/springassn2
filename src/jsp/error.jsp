<!DOCTYPE html>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <h1>Error</h1>
    <p>${errorMessage}</p>
    <c:if test="${not empty errors}">
        <p>The following validation errors occurred:</p>
        <ul>
            <c:forEach var="error" items="${errors}">
                <li>${error.defaultMessage}</li>
            </c:forEach>
        </ul>
    </c:if>
    <br>
    <a href="/employee">Back to List</a>
</body>
</html>
